package ru.am;

import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random rnd = new Random();
        int[] numbers = new int[8];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = rnd.nextInt(11);
        }

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i-1] >= numbers[i]) {
                System.out.println("\nМассив не является строго возрастающей последовательностью.");
                break;
            }
            if (i == numbers.length-1) {
                System.out.println("\nМассив является строго возрастающей последовательностью.");
            }
        }

        for (int i = 0; i < numbers.length; i++) {
            if (i%2 == 1) {
                numbers[i] = 0;
            }
        }

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }

    }
}